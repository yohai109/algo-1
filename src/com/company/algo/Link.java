package com.company.algo;

public class Link {
    int nodeA;
    int nodeB;
    int cost;

    Link(int nodeA, int nodeB, int cost) {
        this.nodeA = nodeA;
        this.nodeB = nodeB;
        this.cost = cost;
    }

    public void print(){
        System.out.println(nodeA + " --> " + nodeB + "\t\t" + cost);
    }
}
