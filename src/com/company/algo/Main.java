package com.company.algo;

import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        // Create graph and print
        Graph generatedGraph = generateGraph(20);
        generatedGraph.print();

        // Find MST for the graph and print
        ArrayList<Link> mst = Prim(generatedGraph);
        printTree(mst);

        // Crete first Link
        Link firstLink = new Link(0, 0, 0);
        firstLink.print();

        // Add new link to tree
        addLink(mst, firstLink);

        // Crete second Link
        Link secondLink = new Link(0, 0, 0);
        secondLink.print();

        // Add new link to tree
        addLink(mst, secondLink);
    }

    public static Graph generateGraph(int numOfVortex) {
        int[][] graph = new int[numOfVortex][numOfVortex];
        Random rand = new Random();

        for (int i = 0; i < numOfVortex; i++) {
            graph[i][i] = 0;
            for (int j = 0; j < i; j++) {
                graph[i][j] = rand.nextInt(numOfVortex - 1);
                graph[j][i] = graph[i][j];
            }
        }
        return new Graph(graph);
    }

    public static ArrayList<Link> Prim(Graph graph) {
        int i;
        int j;
        int currNode;
        int x;
        int y;

        boolean[] Reached = new boolean[graph.num_nodes]; // array to keep track of the reached nodes
        ArrayList<Link> tree = new ArrayList<>(graph.num_nodes);

        // starting vertex
        Reached[0] = true;

        //setting other vertices as unreached
        for (currNode = 1; currNode < graph.num_nodes; currNode++) {
            Reached[currNode] = false;
        }

        tree.add(new Link(0, 0, 0));      // No edge for node 0

        //we iterate for n-1 nodes that haven't been reached yet
        for (currNode = 1; currNode < graph.num_nodes; currNode++) {
            x = 0;
            y = 0;

            for (i = 0; i < graph.num_nodes; i++) {
                for (j = 0; j < graph.num_nodes; j++) {
                    //update the MST with the minimum cost Link
                    if (Reached[i] && !Reached[j] && graph.LinkCost[i][j] < graph.LinkCost[x][y]) {
                        x = i;
                        y = j;
                    }
                }
            }

            // add the min cost link to tree
            tree.add(new Link(y, x, graph.LinkCost[x][y]));
            Reached[y] = true;
        }

        return tree;
    }

    public static void printTree(ArrayList<Link> tree) {
        for (Link link : tree) {
            link.print();
        }
    }

    public static void addLink(ArrayList<Link> tree, Link newLink) {
    }
}
