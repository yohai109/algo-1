package com.company.algo;

public class Graph {

    // Representation of the graph

    public static final int infinite = Integer.MAX_VALUE;

    int[][] LinkCost; // graph matrix
    int num_nodes; // number of nodes

    // constructor takes in a matrix as its input
    Graph(int[][] mat) {
        int i, j;

        num_nodes = mat.length;

        LinkCost = new int[num_nodes][num_nodes];

        // copying the weights to LinkCost matrix
        for (i = 0; i < num_nodes; i++) {
            for (j = 0; j < num_nodes; j++) {
                if (mat[i][j] == 0) {
                    LinkCost[i][j] = infinite;
                } else {
                    LinkCost[i][j] = mat[i][j];
                }
            }
        }
    }

    public void print() {
        for (int i = 0; i < num_nodes; i++) {
            for (int j = 0; j < num_nodes; j++) {
                System.out.print((LinkCost[i][j] != infinite ? (LinkCost[i][j]) : (0)) + "\t");
            }
            System.out.println();
        }
    }
}