from Graph import Graph
from Link import Link
import random


def generate_gtraph(size: int):
    graph = [0] * size
    for i in range(0, size):
        graph[i] = [0] * size
        for j in range(0, i):
            graph[i][j] = random.randint(0, size - 1)
            graph[j][i] = graph[i][j]

    return Graph(graph)


def prim(graph: Graph):
    i = 0
    j = 0
    curr_node = 0
    x = 0
    y = 0

    reached = [False] * graph.num_of_nodes
    tree = []

    reached[0] = True

    tree.append(Link(0, 0, 0))

    for curr_node in range(1, graph.num_of_nodes):
        x = 0
        y = 0

        for i in range(0, graph.num_of_nodes):
            for j in range(0, graph.num_of_nodes):
                if (
                    reached[i]
                    and not reached[j]
                    and graph.link_cost[i][j] < graph.link_cost[x][y]
                ):
                    x = i
                    y = j

        tree.append(Link(x, y, graph.link_cost[x][y]))
        reached[y] = True

    return tree


def print_tree(tree):
    for curr_link in tree:
        curr_link.print_link()


def add_link(tree, new_link: Link):
    """
    docstring
    """
    pass


def main():
    NODES_IN_GRAPH = 20

    # Create graph and print
    graph = generate_gtraph(NODES_IN_GRAPH)
    graph.print_graph()

    # Find MST for the graph and print
    mst = prim(graph)
    print_tree(mst)

    # Crete first Link
    first_link = Link(0, 0, 0)
    first_link.print_link()

    # Add new link to tree
    add_link(mst, first_link)

    # Crete second Link
    second_link = Link(0, 0, 0)
    second_link.print_link()

    # Add new link to tree
    add_link(mst, second_link)


if __name__ == "__main__":
    main()