import sys


class Graph:
    INFITITY = sys.maxsize

    def __init__(self, matrix):
        self.num_of_nodes = len(matrix)
        self.link_cost = [None] * self.num_of_nodes

        for i in range(0, self.num_of_nodes):
            self.link_cost[i] = [None] * self.num_of_nodes
            for j in range(0, self.num_of_nodes):
                if matrix[i][j] == 0:
                    self.link_cost[i][j] = self.INFITITY
                else:
                    self.link_cost[i][j] = matrix[i][j]

    def print_graph(self):
        for i in range(0, self.num_of_nodes):
            for j in range(0, self.num_of_nodes):
                print(
                    (self.link_cost[i][j] if self.link_cost[i][j] != self.INFITITY else "0"),
                    end ="\t"
                )
            print()
