class Link:
    def __init__(self, node_a, node_b, cost):
        self.node_a = node_a
        self.node_b = node_b
        self.cost = cost

    def print_link(self):
        print(self.node_a, " -> ", self.node_b, "\t", self.cost)
