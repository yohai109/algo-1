# יוחאי כנעני 207063249
#  ים ליבמן 205935463
class Graph:

    def __init__(self, graph):
        self.graph = graph
        self.ROW = len(graph)

    # Using BFS as a searching algorithm
    def searching_algo_BFS(self, s, t, parent):

        visited = [False for i in range(self.ROW + 1)]
        queue = []

        queue.append(s)
        visited[s] = True

        while queue:
            current = queue.pop(0)

            for ind, val in enumerate(self.graph[current]):
                if (not visited[ind]) and val > 0:
                    queue.append(ind)
                    visited[ind] = True
                    parent[ind] = current

        return visited[t]

    def print(self):
        for i in range(len(self.graph)):
            for j in range(len(self.graph)):
                print(self.graph[i][j], end="\t")
            print()
