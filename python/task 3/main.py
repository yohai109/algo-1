# יוחאי כנעני 207063249
#  ים ליבמן 205935463
from Graph import Graph
import random


def main():
    graph_size = 20
    graph = generate_graph(graph_size)
    g = Graph(graph)

    # set source and destination
    source = random.randint(0, graph_size)
    dest = random.randint(0, graph_size)

    # make sure destination != source
    while dest == source:
        dest = random.randint(0, graph_size)

    # printing the graph
    g.print()

    # print num of strangers
    print()
    print(find_strangers(g, source, dest))


def ford_fulkerson(graph, source, dest):
    parent = [-1] * graph.ROW
    max_flow = 0

    while graph.searching_algo_BFS(source, dest, parent):

        path_flow = float("Inf")
        s = dest
        while s != source:
            path_flow = min(path_flow, graph.graph[parent[s]][s])
            s = parent[s]

        # Adding the path flows
        max_flow += path_flow

        # Updating the residual values of edges
        v = dest
        while v != source:
            u = parent[v]
            graph.graph[u][v] -= path_flow
            graph.graph[v][u] += path_flow
            v = parent[v]

    return max_flow


def find_strangers(graph: Graph, s, t):
    graph_size = len(graph.graph)
    temp_graph = [[0 for i in range(graph_size)] for j in range(graph_size)]
    for i in range(graph_size):
        for j in range(i):
            weight = 1 if graph.graph[i][j] != 0 else 0
            temp_graph[i][j] = weight
            temp_graph[j][i] = weight
    return ford_fulkerson(Graph(temp_graph), s, t)


def generate_graph(size):
    graph = [[0 for i in range(size)] for j in range(size)]
    for i in range(size):
        for j in range(i):
            weight = 0 if i == j else random.randint(0, size - 1)
            graph[i][j] = weight
            graph[j][i] = weight
    return graph


if __name__ == '__main__':
    main()
